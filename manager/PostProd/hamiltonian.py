from itertools import product as itp
import math

import numpy as np
import scipy.linalg as ln
from abisuite import HARTREE_TO_EV, AbinitDMFTEigFile
from scipy.interpolate import RegularGridInterpolator as GridInt
import regex as re

from .bases import PostProdBase
from .kptmesh import KptMesh
from .projectors import Projectors
from manager.PostProd import plotting
from .plotting import PlotParams


class Hk(PostProdBase):
    _loggername = "postprod.hamiltonian"

    def __init__(self, nkpt, dim, mesh=None, diag=False, **kwargs):
        """Object that represents a Hamiltonian depending of the kpt.

        Parameters
        ----------
        nkpt : int
               The number of kpts.
        dim : int
              The number of dimensions.
        mesh : KptMesh
               Mesh on which the hamiltonian is defined.
        diag : bool
               Specifies whether the hamiltonian is diagonal or not. If yes,
               it is treated as a vector. Else a matrix.
        """
        super().__init__(**kwargs)
        self.diag = diag
        if diag:
            self.hamiltonian = np.zeros((nkpt, dim), dtype=complex)
        else:
            self.hamiltonian = np.zeros((nkpt, dim, dim), dtype=complex)
        self.nkpt = nkpt
        self.dim = dim
        self.mesh = None
        if mesh:
            if not isinstance(mesh, KptMesh):
                raise TypeError("The mesh must be given as a KptMesh type!")
            self.mesh = mesh

    def read(self, _file):
        """Read the Hamiltonian from a file.

        Parameters
        ----------
        _file : str
                The file's path.
        """

        self._logger.info('Loading Hamiltonian from file ' + _file + '.')
        if _file.endswith(".Hk"):
            self._read_from_hkfile(_file)
        elif _file.endswith(".eig"):
            self._read_from_eigfile(_file)
        elif _file.endswith("_EIG"):
            self._read_from_EIGfile(_file)
        else:
            raise ValueError("Cannot read the file type from filename.")
        self._logger.info('Loading done.')

    def _read_from_hkfile(self, _file):
        with open(_file, 'r') as f:
            lines = f.readlines()
        dim = int(lines[0])
        nkpt = int(lines[1])

        # do some checkups
        assert dim == self.dim
        assert nkpt == len(self)

        for k in range(nkpt):
            i = 0
            hk = lines[k + 2].split()
            if self.diag:
                for n in range(dim):
                    ind = 2 * i
                    self[k, n] = (float(hk[ind]) + 1j * float(hk[ind + 1]))
                    i += dim + 1
            else:
                for n in range(dim):
                    for m in range(dim):
                        ind = 2 * i
                        self[k, n, m] = (float(hk[ind]) +
                                         1j * float(hk[ind + 1]))
                        i += 1

    def _read_from_eigfile(self, _file):
        with AbinitDMFTEigFile.from_file(_file) as eig_file:
            # check that everything matches
            if self.nkpt != eig_file.nkpt:
                raise ValueError("Number of kpts does not match.")
            if self.dim != eig_file.nband:
                raise ValueError("Number of bands does not match.")
            # for now we only do non spin polarized calculations. Thus,
            # we do not take into account the spins.
            data = eig_file.eigenvalues
            if len(np.shape(eig_file.eigenvalues)) > 2:
                # data is spin polarised, print warning and select first row.
                self._logger.warning("Data is spin polarized. Taking only "
                                     "first spin data.")
                data = data[0]
        # filling self and converting to eV
        data = np.array(data) * HARTREE_TO_EV
        for ikpt, kpt in enumerate(data):
            if self.diag:
                self[ikpt] = kpt
            else:
                np.fill_diagonal(self[ikpt, :, :], kpt)

    def _read_from_EIGfile(self, _file):
        with open(_file, 'r') as f:
            lines = f.readlines()
        i = 0
        for line in lines[2:]:
            j = i % (math.ceil(self.dim/8) + 1)
            if j:
                k = int(i / (math.ceil(self.dim/8) + 1))
                for n, l in enumerate(line.split()):
                    self[k, 8*(j-1)+n, 8*(j-1)+n] = float(l) * HARTREE_TO_EV
            i += 1

    def write(self, hk_out='outfile.Hk', klist_out='outfile.klist'):
        """Writes the hamiltonian data into a file following a Kpt Mesh.

        Parameters
        ----------
        hk_out : str, optional
                 The hamiltonian file path.
        klist_out : str, optional
                    The kpt list file path that matches the hamiltonian.
        """
        if not isinstance(self.mesh, KptMesh):
            raise ValueError("This function is built"
                             " to work with a KptMesh only.")
        if self.mesh.nkpt > self.nkpt:
            raise ValueError("The mesh has more k pts"
                             " than the Hk object, which is impossible.")
        self._logger.info("Writing Hk.hamiltonian in the file " + hk_out +
                          " and the klist in the file " + klist_out + ".")
        # f_Hk = open(Hk_out, 'w')
        f_Hk = []
        f_Hk.append("%d\n" % self.dim)
        f_Hk.append("%d\n" % self.mesh.nkpt)

        # f_kl = open(klist_out, 'w')
        f_kl = []
        f_kl.append("%d\n" % self.mesh.nkpt)

        for nx, ny, nz in itp(range(2*self.mesh.n[0]),
                              range(2*self.mesh.n[1]),
                              range(2*self.mesh.n[2])):
            if self.mesh[nx, ny, nz] != -1:

                m = self.mesh.fromNgetM([nx, ny, nz])
                f_kl.append("%.12f\t%.12f\t%.12f\n" %
                            (m.item(0), m.item(1), m.item(2)))

                # h0 = [[[0, 0] for n in range(self.dim)]
                #        for m in range(self.dim)]
                hk = ""
                for n in range(self.dim):
                    for m in range(self.dim):
                        o = self[self.mesh[nx, ny, nz]]
                        hk += ("%.10f\t%.10f\t" % (o[n, m].real, o[n, m].imag))
                hk += '\n'
                f_Hk.append(hk)
        with open(hk_out, 'w') as f:
            f.writelines(f_Hk)
        with open(klist_out, 'w') as f:
            f.writelines(f_kl)
        self._logger.info('Writing done.')

    def from_band_to_orb_basis(self, mesh=None, proj=None):
        # Make sure there is a mesh of type KptMesh and sets it as mesh.
        if not mesh or not isinstance(mesh, KptMesh):
            if not isinstance(self.mesh, KptMesh):
                raise TypeError("Mesh most be a Kpt Mesh.")
            mesh = self.mesh

        # Make sure there is a proj of type Projectors.
        if not proj:
            raise ValueError("The proj argument must be given to change "
                             "basis.")
        if not isinstance(proj, Projectors):
            raise TypeError("The proj argument must be of type Projectors.")

        # The change of basis requieres a well defined set of orbitals.
        if not len(mesh.sgroup.orbs):
            raise ValueError("Orbitals are not defined for the rotation of"
                             " the projectors! Define them using mesh.sgroup."
                             "SetOrbs().")

        # Temporary hamiltonian for each atoms
        new_eig = [Hk(mesh.nkpt, proj.norb*proj.nspin)
                   for at in range(proj.natom)]

        for k in range(mesh.nkpt):
            for at in range(proj.natom):
                for spin in range(proj.nspin):
                    for spin2 in range(proj.nspin):
                        s1i = proj.norb*spin
                        s1f = proj.norb*(spin+1)
                        s2i = proj.norb*spin2
                        s2f = proj.norb*(spin2+1)
                        p1 = proj[k, at, spin]
                        p2 = np.transpose(np.conj(proj[k, at, spin2]))
                        a = p1.dot(self[k]).dot(p2)
                        new_eig[at][k][s1i:s1f, s2i:s2f] = a
        return new_eig

    def full_mesh_hamilt(self, mesh=None, proj=None, testmode=False,
                         write=None, sgroups=None):
        """Returns the hamiltonian on a full 3D mesh. If projectors included,
           the hamiltonian will be basis changed.

        Parameters
        ----------
        mesh : kmesh.KptMesh
               The mesh in mandatory because the hamiltonian is returned as an
               array with the shape of the mesh, with the value of the
               hamiltonian on each k point site. If the mesh is not given here
               as an argument, it could have been given while initiializing the
               hamiltonian.
        proj : projectors.Projectors
               If it is desired that the hamiltonian is returned in the orbital
               basis, give the projectors as an argument, otherwise keep it
               None.
        testmode : bool
                   If you want to check the incertainty of your symmetrization,
                   give a hamiltonian (and projectors) on a full mesh and check
                   the consistency with regard to symmetrization.
        write : str array
                If you want to write down the resulting Hamiltonian instead of
                returning it, give the file for .Hk first and .klist second.
        """
        np.set_printoptions(threshold=np.inf, linewidth=np.inf, precision=1)

        # Make sure there is a mesh of type KptMesh and sets it as mesh.
        if not mesh or not isinstance(mesh, KptMesh):
            if not isinstance(self.mesh, KptMesh):
                raise TypeError("Mesh most be a Kpt Mesh.")
            mesh = self.mesh

        # If proj is not None, take the orbital dimensions for the returned
        # hamiltonian and initiate projectors on a 3D tensor as the mesh.
        dim = self.dim
        mproj = None
        if proj and isinstance(proj, Projectors):
            print("Using projectors in full_mesh_hamilt function.")
            natom = proj.natom
            dim = sum(proj.norb)*proj.nspin
            # tot_norb = sum(proj.norb)
            pre_norb = []
            i = 0
            for a in range(proj.natom):
                pre_norb.append(i)
                i += proj.norb[a]*proj.nspin
            pre_norb.append(i)
            mproj = []
            for a in range(proj.natom):
                mproj.append(np.zeros((2*mesh.n[0], 2*mesh.n[1], 2*mesh.n[2],
                                       proj.nspin*proj.norb[a], self.dim),
                                      dtype=complex))
            if sgroups is None:
                # The change of basis requieres a set of orbitals well defined
                if not len(mesh.sgroup.orbs):
                    raise ValueError("Orbitals are not defined for the "
                                     "rotation of projectors! Define "
                                     "them using mesh.sgroup.SetOrbs().")
                else:
                    sgroups = [mesh.sgroup for a in range(proj.natom)]
        else:
            natom = 1
            sgroups = [mesh.sgroup]

#       #############
#       # Fill the band hamiltonian and possibly the mesh_projectors with
#       ### given information from the mesh
#       #############
#       # START ###
#       #############

        if mproj is not None:
            for nx, ny, nz in itp(range(2*mesh.n[0]),
                                  range(2*mesh.n[1]),
                                  range(2*mesh.n[2])):
                kpt = mesh.mesh[nx, ny, nz]
                if kpt != -1:
                    for at in range(natom):
                        for sp in range(proj.nspin):
                            sp1 = sp*proj.norb[at]
                            sp2 = (sp+1)*proj.norb[at]
                            tempproj = proj.projectors[at][kpt][sp]
                            mproj[at][nx, ny, nz, sp1:sp2] = tempproj

        # nk = mesh.nkpt
        # nosym = 0
        # pre_mesh = 0

        # There is a phase factor that can differ in projectors
        if testmode:
            Ns = []
            for i in range(2**self.dim):
                N = np.diag(np.ones(self.dim))
                for j in range(2**self.dim):
                    if not j:
                        if i % 2:
                            N[j][j] = -1
                    else:
                        if (i % 2**(j+1)) > 2**j-1:
                            N[j][j] = -1
                Ns.append(N)

        # Use the symmetries of the sgroup to fill the band_hamilt and mproj
        # Variables for %
        numk = 2*mesh.n[0]*2*mesh.n[1]*2*mesh.n[2]
        numdone = 0
        prct = 0

        # Function for returning the right spin labels in terms of
        # the spin generators, from a name of SGroup.
        def labelSpinToGens(text):
            S = np.diag([1, 1])
            for op in re.split(r"(\W)", text):
                if op == "":
                    break
                if op != "*":
                    S = S.dot(self.mesh.sgroup.spinGen[op])
            return S

        # Loop on all points of the mesh
        for nx, ny, nz in itp(range(2*mesh.n[0]),
                              range(2*mesh.n[1]),
                              range(2*mesh.n[2])):
            prct_now = math.floor(numdone / numk * 100)
            if prct_now > prct:
                print("[%d/100]" % prct_now)
                prct = prct_now
            numdone += 1
            # If the point is in the FBZ and ((none set and not in testmode)
            # or (set and in testmode))
            if (mesh.in_FBZ(mesh.fromNgetM([nx, ny, nz]))
                and ((not testmode and mesh[nx, ny, nz] == -1)
                     or (testmode and mesh[nx, ny, nz] != -1))):
                # Setting stats
                # filled = 0
                bad_energy = 0
                bad_proj = 0

                # Write the point in tensor index
                n = np.transpose(np.matrix((nx, ny, nz)))
                # Convert in (kx,ky,kz) basis
                k = ln.inv(mesh.S).dot(n-mesh.N0)

                # Loop on the symmetry matrices
                for g in sgroups[0].gs:
                    # Apply symmetry
                    k2 = sgroups[0].GtoMatrix(g).dot(k)
                    # Rewrite in tensor index
                    n2 = mesh.S.dot(k2)+mesh.N0
                    n2 = mesh.translate_in_FBZ([n2.item(0), n2.item(1),
                                                n2.item(2)])
                    n2x = round(n2[0])
                    n2y = round(n2[1])
                    n2z = round(n2[2])

                    # If n2, symmetry-related to n1, is none empty
                    if mesh[n2x, n2y, n2z] != -1:
                        for at in range(natom):
                            G = sgroups[at].GtoOrb(g)
                            if mproj is not None:
                                if proj.nspin > 1:
                                    S = self.mesh.sgroup.gO[g]
                                    S = labelSpinToGens(S)
                                    G = np.kron(S, G)
                                    # print("g: \n", g)
                                    # print("S: \n", S)
                                    # print("G: \n", G)
                                    # print()
                            # No testmode = assign
                            if not testmode:
                                mesh[nx, ny, nz] = mesh[n2x, n2y, n2z]
                                if mproj is not None:
                                    # for sp in range(proj.nspin):
                                    #     mproj2 = mproj[at][n2x, n2y, n2z, sp]
                                    #     new_proj = G.dot(mproj2)
                                    #     mproj[at][nx, ny, nz, sp] = new_proj
                                    mproj2 = mproj[at][n2x, n2y, n2z]
                                    new_proj = G.dot(mproj2)
                                    mproj[at][nx, ny, nz] = new_proj
                            elif mproj is None:
                                break
                            else:
                                prec = 5

                                # Check energies
                                ham1 = self[mesh[nx, ny, nz]]
                                ham2 = self[mesh[n2x, n2y, n2z]]
                                cond = ham1.round(prec) != ham2.round(prec)
                                if (cond).any():
                                    bad_energy += 1
                                    self._logger.debug("Band energy:")
                                    self._logger.debug(ham1.round(prec))
                                    self._logger.debug(ham2.round(prec))

                                # Check mesh projectors
                                m1 = mproj[nx, ny, nz].round(prec)
                                m2 = mproj[n2x, n2y, n2z].round(prec)

                                condition = (m1 != G.dot(m2)).any()
                                if condition:
                                    prob = 1
                                    # Check phase in band basis
                                    for N in Ns:
                                        cond = (G.dot(m2).dot(N) ==
                                                m1).all()
                                        if cond:
                                            prob = 0
                                            break
                                    if prob:
                                        bad_proj += 1
                                        mess = ("Sym = %s"
                                                % (g))
                                        self._logger.debug(mess)
                                        mess = "Bad projectors:"
                                        self._logger.debug(mess)
                                        self._logger.debug(m1)
                                # for sp in range(proj.nspin):
                                #     cond = (m1[sp] != G.dot(m2[sp])).any()
                                #     if cond:
                                #         prob = 1
                                #         # Check phase in band basis
                                #         for N in Ns:
                                #             cond = (G.dot(m2[sp]).dot(N) ==
                                #                     m1[sp]).all()
                                #             if cond:
                                #                 prob = 0
                                #                 break
                                #         if prob:
                                #             bad_proj += 1
                                #             mess = ("Spin = %d; Sym = %s"
                                #                     % (sp, g))
                                #             self._logger.debug(mess)
                                #             mess = "Bad projectors:"
                                #             self._logger.debug(mess)
                                #             self._logger.debug(m1[sp])
                                #             self._logger.debug(G.dot(m2[sp]))
        # Exit testmode
        if testmode:
            if not bad_energy and not bad_proj:
                return True
            else:
                raise ValueError("Problems with symmetrization in testmode.")
#       ############
#       # STOP ###
#       ############

        # Now we should have everything to compute the band or
        # orbital hamiltonian on the full mesh
        full_nkpt = 0
        final_hamilt = np.zeros((2*mesh.n[0], 2*mesh.n[0], 2*mesh.n[2],
                                 dim, dim), dtype=complex)

        for nx, ny, nz in itp(range(2*mesh.n[0]),
                              range(2*mesh.n[1]),
                              range(2*mesh.n[2])):
            k = mesh[nx, ny, nz]
            if mproj is None:
                if k != -1:
                    full_nkpt += 1
                    if self.diag:
                        final_hamilt[nx, ny, nz] = np.diag(self[k])
                    else:
                        final_hamilt[nx, ny, nz] = self[k]
            else:
                if k != -1:
                    full_nkpt += 1
                    spins_atoms = itp(range(proj.natom),
                                      range(proj.natom))
                    for a1, a2 in spins_atoms:
                        kproj1 = mproj[a1][nx, ny, nz]
                        kproj2 = mproj[a2][nx, ny, nz]

                        kproj2 = np.transpose(np.conj(kproj2))

                        if self.diag:
                            Ho = kproj1.dot(np.diag(self[k])).dot(kproj2)
                        else:
                            Ho = kproj1.dot(self[k]).dot(kproj2)

                        a1i = pre_norb[a1]
                        a1f = pre_norb[a1+1]

                        a2i = pre_norb[a2]
                        a2f = pre_norb[a2+1]

                        final_hamilt[nx, ny, nz, a1i:a1f,
                                     a2i:a2f] = Ho
                    # spins_atoms = itp(range(proj.nspin),
                    #                   range(proj.nspin),
                    #                   range(proj.natom),
                    #                   range(proj.natom))
                    # for sp1, sp2, a1, a2 in spins_atoms:
                    #     kproj = mproj[a1][nx, ny, nz, sp1]
                    #     mproj2 = mproj[a2][nx, ny, nz, sp2]
                    #     kproj2 = np.transpose(np.conj(mproj2))
                    #     if self.diag:
                    #         Ho = kproj.dot(np.diag(self[k])).dot(kproj2)
                    #     else:
                    #         Ho = kproj.dot(self[k]).dot(kproj2)

                    #     a1i = pre_norb[a1]
                    #     s1i = proj.norb[a1]*(sp1)
                    #     s1f = proj.norb[a1]*(sp1+1)

                    #     a2i = pre_norb[a2]
                    #     s2i = proj.norb[a2]*(sp2)
                    #     s2f = proj.norb[a2]*(sp2+1)

                    #     final_hamilt[nx, ny, nz, a1i+s1i:a1i+s1f,
                    #                  a2i+s2i:a2i+s2f] = Ho

        if write is not None and len(write) == 2:
            f_Hk = []
            f_Hk.append("%d\n" % dim)
            f_Hk.append("%d\n" % full_nkpt)

            # f_kl = open(klist_out, 'w')
            f_kl = []
            f_kl.append("%d\n" % full_nkpt)

            for nx, ny, nz in itp(range(2*mesh.n[0]),
                                  range(2*mesh.n[1]),
                                  range(2*mesh.n[2])):
                if mesh[nx, ny, nz] != -1:
                    m = mesh.fromNgetM([nx, ny, nz])
                    f_kl.append("%.12f\t%.12f\t%.12f\n" %
                                (m.item(0), m.item(1), m.item(2)))

                    # h0 = [[[0, 0] for n in range(self.dim)]
                    #        for m in range(self.dim)]
                    hk = ""
                    for n in range(dim):
                        for m in range(dim):
                            o = final_hamilt[nx, ny, nz]
                            hk += ("%.10f\t%.10f\t" % (o[n, m].real,
                                                       o[n, m].imag))
                    hk += '\n'
                    f_Hk.append(hk)
            with open(write[0], 'w') as f:
                f.writelines(f_Hk)
            with open(write[1], 'w') as f:
                f.writelines(f_kl)
            self._logger.info('Writing done.')

        return final_hamilt

    def plotInPlane(self, fermi, cmap=None, cutoff=0., broad=0.05,
                    params=None, fill=False, selfE=None, rgb=None, ikz=0):
        if params is None:
            params = PlotParams()
        if not isinstance(params, PlotParams):
            raise TypeError("Error: params must be of type PlotParams or"
                            " None.")

        if selfE is None:
            selfE = np.zeros((self.dim, self.dim))
        else:
            if len(selfE) != self.dim:
                raise ValueError("Error: selfE must be of the same dimensions "
                                 "the hamiltonian.")
            selfE = np.diag(selfE)

        m = self.mesh
        if m is None:
            raise ValueError("Error: self.mesh needs to be setted.")
        mx, my, mz = m.n[0], m.n[1], m.n[2]

        mu = (fermi + 1j*broad)*np.identity(self.dim)

        A = np.zeros((self.dim, 2*mx, 2*my, 2*mz))
        G = np.zeros((self.dim, self.dim), dtype=complex)
        for k, kpt in enumerate(m.npts):
            G = ln.inv(mu - self[k] - selfE)
            k1x, k1y, k1z = kpt[0], kpt[1], kpt[2]
            for d in range(self.dim):
                A[d][k1x][k1y][k1z] = -1./math.pi*np.imag(G[d][d])

        if fill:
            k_set = itp(range(2*mx), range(2*my), range(2*mz))
            for kx, ky, kz in k_set:
                if m[kx, ky, kz] == -1:
                    k2 = m.translate_in_FBZ([kx, ky, kz])
                    k2x, k2y, k2z = (round(k2[0]), round(k2[1]),
                                     round(k2[2]))
                    for d in range(self.dim):
                        A[d][kx][ky][kz] = A[d][k2x][k2y][k2z]

        print(params["subplots"])

        default = PlotParams()
        default["figsize"] = (10, 10)
        default["subplots"] = [1, 1]

        params.setDefaults(default)

        print(params["subplots"])

        fig, ax = plotting.setFigure(params)

        if isinstance(cutoff, float) or isinstance(cutoff, int):
            cutoff = [cutoff for d in range(self.dim)]
        elif len(cutoff) != self.dim:
            raise ValueError("Error: dimension of cutoff is incorrect.")

        nrows, ncols = params["subplots"][0], params["subplots"][1]

        print(ikz)
        if isinstance(ikz, int):
            ikz = [[ikz for x in range(ncols)] for y in range(nrows)]
        print(ikz)

        rowcols = itp(range(nrows), range(ncols))
        for row, col in rowcols:
            kz = ikz[row][col]
            print("kz = ", kz)
            if params["subplots"][0] != 1:
                if params["subplots"][1] != 1:
                    axe = ax[row][col]
                else:
                    axe = ax[row]
            else:
                if params["subplots"][1] != 1:
                    axe = ax[col]
                else:
                    axe = ax
            if rgb is not None:
                rgb_index = rgb[0][row][col]
                print("rgb_index = ", rgb_index)
                A_rgb = [np.zeros((2*mx, 2*my, 2*mz)) for x in range(3)]
                for a in range(3):
                    if rgb_index[a] is not None:
                        if rgb[1]:
                            A[rgb_index[a]] /= np.amax(A[rgb_index[a]])
                        A_rgb[a] = A[rgb_index[a]]
                if not rgb[2]:
                    prt_A = [[(A_rgb[0][kx, ky, kz], A_rgb[1][kx, ky, kz],
                               A_rgb[2][kx, ky, kz])
                              for ky in range(len(A[0][0]))]
                             for kx in range(len(A[0]))]
                else:
                    prt_A = [[(1-A_rgb[0][kx, ky, kz], 1-A_rgb[1][kx, ky, kz],
                               1-A_rgb[2][kx, ky, kz])
                              for ky in range(len(A[0][0]))]
                             for kx in range(len(A[0]))]

                axe.imshow(prt_A, origin="lower")
            else:
                d = params["indices"][row][col]
                print("indices: ", d)
                if d == "sum":
                    B = np.sum(A, axis=0)
                    A[0] = B
                    d = 0
                prt_A = np.ma.masked_where(A[d][:, :, kz] < cutoff[d],
                                           A[d][:, :, kz])
                if cmap is not None:
                    axe.imshow(prt_A, cmap=cmap[row][col], origin="lower")
                else:
                    axe.imshow(prt_A, origin="lower")

            if params["special1"]:
                plotting.plotSpecial1(axe, m.n, params)
            if params["special2"]:
                plotting.plotSpecial2(axe, m.n, params)

        return fig, ax

    def interpolate(self, mesh2):
        if not isinstance(mesh2, KptMesh):
            raise TypeError("Error: mesh2 must be of type KptMesh.")

        m = self.mesh
        mx, my, mz = m.n[0], m.n[1], m.n[2]
        m2 = mesh2
        m2x, m2y, m2z = m2.n[0], m2.n[1], m2.n[2]
        ndim = self.dim
        meig = np.zeros((2*mx+1, 2*my+1, 2*mz+1, ndim, ndim), dtype=complex)

        q_set = itp(range(2*mx+1), range(2*my+1), range(2*mz+1))

        print("Filling meig")
        for qx, qy, qz in q_set:
            q = m.translate_in_FBZ([qx-1, qy-1, qz-1])
            meig[qx, qy, qz] = self[m[q[0], q[1], q[2]]]
        print("Filled")

        interpol = np.zeros((ndim, ndim), dtype=GridInt)

        print("Interpolating")
        for l1, l2 in itp(range(ndim), range(ndim)):
            linx = np.linspace(-1, 2*mx-1, 2*mx+1)
            liny = np.linspace(-1, 2*my-1, 2*my+1)
            linz = np.linspace(-1, 2*mz-1, 2*mz+1)
            interpol[l1, l2] = GridInt((linx, liny, linz),
                                       meig[:, :, :, l1, l2])

        int_meig = np.zeros((2*m2x, 2*m2y, 2*m2z,
                             ndim, ndim), dtype=complex)

        q_set2 = itp(range(2*m2x), range(2*m2y),
                     range(2*m2z))
        eig2 = np.zeros((m2x*m2y*m2z*2, ndim, ndim), dtype=complex)
        i = 0
        print("New eig")
        for qx, qy, qz in q_set2:
            q2x = 1.*(qx-m2.N0.item(0))*m.n[0]/m2.n[0]+m.N0.item(0)
            q2y = 1.*(qy-m2.N0.item(1))*m.n[1]/m2.n[1]+m.N0.item(1)
            q2z = 1.*(qz-m2.N0.item(2))*m.n[2]/m2.n[2]+m.N0.item(2)
            if m2.in_FBZ(m2.fromNgetM([qx, qy, qz])):
                m2[qx, qy, qz] = i
                m2.npts.append([qx, qy, qz])
                for l1, l2 in itp(range(ndim), range(ndim)):
                    v = interpol[l1, l2](np.array([q2x, q2y, q2z]))
                    eig2[i, l1, l2] = v
                    int_meig[qx, qy, qz, l1, l2] = v
                i += 1
        m2.nkpt = len(m2.npts)

        Eig2 = Hk(m2.nkpt, ndim, m2)
        Eig2.hamiltonian = eig2
        return Eig2

    def returnVelocity(self, direction, prefac=2):
        if direction not in ["x", "y"]:
            raise Exception("Direction only works for x or y.")

        ndim = self.dim
        m = self.mesh

        hk1 = Hk(m.nkpt, ndim, m)
        hk2 = Hk(m.nkpt, ndim, m)
        for k, kpt in enumerate(m.npts):
            kx, ky, kz = kpt[0], kpt[1], kpt[2]
            if direction == "x":
                k1 = m.translate_in_FBZ([kx+1, ky, kz])
                k2 = m.translate_in_FBZ([kx-1, ky, kz])
            if direction == "y":
                k1 = m.translate_in_FBZ([kx, ky+1, kz])
                k2 = m.translate_in_FBZ([kx, ky-1, kz])
            k1 = m[k1[0], k1[1], k1[2]]
            k2 = m[k2[0], k2[1], k2[2]]
            hk1[k1] = self[k]
            hk2[k2] = self[k]

        vel = Hk(m.nkpt, ndim, m)
        vel[:] = (hk1[:] - hk2[:])/prefac
        return vel

    def __len__(self):
        return len(self.hamiltonian)

    def _get_data(self):
        return self.hamiltonian
