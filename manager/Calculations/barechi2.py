from manager.Calculations import Calculation
from manager.PostProd import TwoPropagator, plotting
from manager.PostProd import PlotParams
from manager.PostProd import KptMesh
from colorama import Fore, Back, Style
import colorama
import os
import re
import numpy as np


class Barechi2(Calculation):

    def __init__(self, system, conn=None, id=-1):
        Calculation.__init__(self, system, conn)
        self.table = "barechi2"
#        self.prevTable = ["barechi1", "dmft"]
#        self.prevCalc = Barechi1(system)

        if id != -1:
            self.id = id
            self.Load(True)

        self.params = {"qts": None, "chempot": None, "niwn": None,
                       "nivn": None, "temp": None, "norb": None, "nspin": None}
        fileInput = ("%s \t\t# case name\n" % system +
                     "0 \t\t# Scheme name; 0 for RPA and 1 for DMFT\n" +
                     "T \t\t# Interacting system\n" +
                     "T \t\t# Q-list\n" +
                     "\n" +
                     "1 \t\t# Num spin\n" +
                     "1 \t\t# Num atom\n" +
                     "!(norbspin) \t\t# Num orbitals\n" +
                     "T \t\t# Is included\n" +
                     "T \t\t# Local Interaction\n" +
                     "\n" +
                     "$(chempot) \t\t # Chemical Potential\n" +
                     "$(temp) \t\t # Inverse Temperature\n" +
                     "\n" +
                     "$(niwn) \t\t# ChiNiwnMax (fermionic)\n" +
                     "$(nivn) \t\t# ChiNivnMax (Bosonic)\n")

        def norbspin():
            return str(self.params["norb"]*self.params["nspin"])
        self.funcParams = {"norbspin": norbspin}

        filePath = re.match(r"^(.+/)\w+\.\w+$",
                            os.path.realpath(__file__)).group(1)

        with open("%s/Barechi2/Input.dat" % filePath, "r") as f:
            fileInput = f.read()
        fileInput = re.sub(r"\*\(system\)", system, fileInput)

        self.tree = {"Input/": {"Input.dat": fileInput,
                                "[%s.klist]" % system: None,
                                "[%s.qlist]" % system: None,
                                "[%s.Hk]" % system: None
                                },
                     "Output/": {},
                     "Figures/": {}
                     }

    def UpdateGammaphUJ(self, U, J, S, Sp):
        '''
        Function to update interactions parameter based on a descent method.
        You can proof with these equations that if you converge numerically,
        you necessarily converge to desired stoner factor.
        You usually need from 3 to 8 updates to converge.
        :param U: old U
        :param J: old J
        :param S: targeted S^M
        :param Sp: previous S^M
        :return: new U, new J
        '''
        newU = U*(1+S-Sp)
        newJ = (J/U)*newU
        return newU, newJ

    def AutoDress(self, JsU, target, grid, spgroup, margin=0.0009,
                  output='default', delete=False, test=False):
        '''
        Function to autodress a barechi2 calculation given a magnetic stoner
        target and a particular J/U.
        :param JsU: The value of J/U (easier to reach convergence)
        :param target: Desired magnetic stoner factor
        :param grid: To properly define your baresusph.
        That's why I said that it could be a good idea to add a column
        "spgroup" and "grid" to define a Barechi2 calculation.
        :param spgroup: See above.
        :return:
        To do : connect this with sbatch object to autodress many different
                    calculations at the same time (parallelise ?)
                clean the useless calculations obtained after convergence has
                    been reached.
                Directly launch Eliash after Dressing.
        '''
        from .dressed import Dressed

        sup = self.system+'.qlist'
        mesh_path = self.conn.groundDir + self.cells["barechi2.barechi2dir"] \
                                        + 'Input/' + sup
        mesh = KptMesh.read(mesh_path, grid, spgroup, _type="klist")
        chi = self.ReturnBareSusph(mesh.nkpt)
        idmax_0 = self.conn.select("select MAX(dressedid) from Dressed")[0][0]

        # First random calculation
        d, sM = self.launchDressing(1/2, JsU/2, chi)

        # Converge
        j = 1
        while np.abs(sM-target) > margin:
            lastid = self.conn.select("select MAX(dressedid) from Dressed")
            lastid = lastid[0][0]
            oldU = self.conn.select("select dressedU from Dressed \
            where dressedid=%d" % lastid)[0][0]
            oldJ = self.conn.select("select dressedJ from Dressed \
            where dressedid=%d" % lastid)[0][0]
            newU, newJ = self.UpdateGammaphUJ(oldU, oldJ, target, sM)
            d, sM = self.launchDressing(newU, newJ, chi)
            j += 1
            if test:
                break

        # Delete prev calculations
        if delete:
            idmax_1 = self.conn.select("select MAX(dressedid) from Dressed")
            idmax_1 = idmax_1[0][0]
            if test:
                idmax_1 += 1
            ids_del = range(idmax_0, idmax_1)[1:]
            for i in ids_del:
                calc = Dressed(self.system, self.conn, i)
                calc.EraseCalculation(all=True)

        # Builing of log file
        if output == 'default':
            output = self.conn.groundDir + self.cells["barechi2.barechi2dir"] \
                                         + 'log_%0.2f_%0.2f' % (JsU, target)
        with open(output, 'w') as log:
            log.write("Convergence has been reached in : %d iterations.\n"
                      "Final parameters for this dressing calculation were :\n"
                      "DressedU = %0.8f\n"
                      "DressedJ = %0.8f\n"
                      "J/U = %0.4f\n"
                      "Magnetic Stoner factor = %0.5f\n"
                      "dressedid = %d\n"
                      % (j, newU, newJ, newJ / newU, sM, lastid + 1))

    def launchDressing(self, U, J, chi):
        '''
        Function to launch a dressing calculation.
        :param U: Intra-orbital interaction
        :param J: Hund's-coupling
        :param chi: It's not quite obvious what is the mesh and what is the
        baresusph on a particular grid. I personally write the grid in "qpts"
        columns (Barechi2) but with much more info. It might be a good idea to
        add columns "grid", "spgroup" to properly define a barechi calculation.
        :return: It returns the magnetic stoner factor.
        '''
        d = self.SetDressed()

        d.SetBaseDir(self.cells["barechi2.barechi2dir"] + 'Dressed/')
        d_params = {"dressedU": U,
                    "dressedJ": J,
                    "dressedUp": U - 2 * J,
                    "dressedUpp": U - 3 * J,
                    "dressedJp": J,
                    "miwn": 64,
                    "mivn": 2}
        d.SetParams(params=d_params, verify=False)
        # Make the folder tree with need links
        d.MakeTree()
        # Load the full history of dressed: join all preceeding tables
        d.Load(True)

        d.Dressing(chi0=chi)

        lastid = self.conn.select("select MAX(dressedid) from Dressed")[0][0]
        sM = self.conn.select("select stonerM from Dressed \
        where dressedid=%d" % lastid)[0][0]

        return d, sM

    def CreateTable(self):
        r"""Create the database for the BareChi2 object. Needs to have
        a self.conn defined. """
        self.testConn()

        query = ("CREATE TABLE barechi2 ( "
                 "barechi2id INTEGER NOT NULL PRIMARY KEY, "
                 "qpts CHAR(255) NOT NULL, "
                 "chempot REAL, "
                 "niwn INTEGER, "
                 "nivn INTEGER, "
                 "barechi2dir CHAR(255) NOT NULL, "
                 "temp REAL, "
                 "norb INTEGER, "
                 "nspin INTEGER, "
                 "nqpath INTEGER, "
                 "status CHAR(255) )")
        self.conn.Query("execute", query)

    def InsertNew(self, values):
        """Insert new calculation in the barechi2 table."""
        self.testConn()

        keys = ["barechi2id", "barechi1id", "qpts", "chempot", "niwn", "nivn",
                "barechi2dir", "temp", "norb", "nspin", "nqpath", "status"]
        txt_keys, txt_values = "(", "("
        for v, val in enumerate(values):
            if val not in keys:
                raise Exception("Unrecgnozed key '%s'." % val)
            if v != 0:
                txt_keys += ", "
                txt_values += ", "
            txt_keys += val
            txt_values += str(values[val])
        txt_keys += ")"
        txt_values += ")"

        query = ("INSERT INTO barechi2 %s VALUES %s" % (txt_keys, txt_values))
        print(query)
        self.id = self.conn.Query("insert", query)
        print("self.id: ", self.id)
        self.Load(True)

    def ReturnBareSusph(self, nkpt):
        if self.id == -1:
            raise("Error: No id.")

        susDir = self.conn.groundDir + self.cells["barechi2.barechi2dir"]
        norb = self.cells["barechi2.norb"]
        nspin = self.cells["barechi2.nspin"]
        nivn = self.cells["barechi2.nivn"]

        if nspin == 1:
            spind = True
        elif nspin == 2:
            spind = False
        else:
            raise ValueError("nspin != [0, 1] not implemented.")
        chi0 = TwoPropagator(nspin*norb, nivn + 1, nkpt, spindiag=spind)
        chi0.Re.Read("%sOutput/%s.ReBareSusph" % (susDir, self.system),
                     _skip=2)
        chi0.Im.Read("%sOutput/%s.ImBareSusph" % (susDir, self.system),
                     _skip=2)

        return chi0

    def ReturnBareSuspp(self, nkpt):
        if self.id == -1:
            raise("Error: No id.")

        susDir = self.conn.groundDir + self.cells["barechi2.barechi2dir"]
        norb = self.cells["barechi2.norb"]
        nspin = self.cells["barechi2.nspin"]
        niwn = 0

        if nspin == 1:
            spind = True
        elif nspin == 2:
            spind = False
        else:
            raise ValueError("nspin != [0, 1] not implemented.")
        chi0 = TwoPropagator(nspin*norb, niwn + 1, nkpt, spindiag=spind)
        chi0.Re.Read("%sOutput/%s.ReBareSuspp" % (susDir, self.system),
                     _skip=2)
        chi0.Im.Read("%sOutput/%s.ImBareSuspp" % (susDir, self.system),
                     _skip=2)
        return chi0

    def ReturnBareChipp(self, nkpt):
        if self.id == -1:
            raise("Error: No id.")

        susDir = self.conn.groundDir + self.cells["barechi2.barechi2dir"]
        norb = self.cells["barechi2.norb"]
        nspin = self.cells["barechi2.nspin"]
        nivn = self.cells["barechi2.nivn"]
        niwn = int(nivn/2) + 1

        if nspin == 1:
            spind = True
        elif nspin == 2:
            spind = False
        else:
            raise ValueError("nspin != [0, 1] not implemented.")
        chi0 = TwoPropagator(nspin*norb, niwn, nkpt, spindiag=spind)
        chi0.Re.Read("%sOutput/%s.ReBareChipp" % (susDir, self.system),
                     _skip=2)
        chi0.Im.Read("%sOutput/%s.ImBareChipp" % (susDir, self.system),
                     _skip=2)
        return chi0

    def SetDressed(self):
        from .dressed import Dressed
        # You need a valid calculation
        if self.id == -1:
            raise("%sError: You cannot plot from this calculation because it"
                  " is not defined in the database. Please load it using"
                  " self.Load(id).%s" %
                  (Fore.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        if len(self.cells) == 0:
            self.Load()

        dressed = Dressed(self.system, self.conn)
        dressed.prevId = self.id
        dressed.prevCalc = self

        return dressed

    # From a calculation, select the kind of extension,
    # give plotting parameters
    def PlotBareSusPath(self, plotParams=None):
        # Initialize the color code for printing
        colorama.init()

        # You need a valid calculation
        if self.id == -1:
            print("%sError: You cannot plot from this calculation because it"
                  " is not defined in the database. Please load it using"
                  "self.Load(id).%s" % (Back.RED + Fore.BLACK,
                                        Style.RESET_ALL))
            return ""
        if len(self.cells) == 0:
            self.cells = self.Load(True)

        # Find parameters constructing the object
        nqpath = self.cells["barechi2.nqpath"]
        nfreqs = self.cells["barechi2.nivn"] + 1

        # Setting default plotting parameters
        default = PlotParams()
        default.params["subplots"] = [2, 2]
        default.params["indices"] = [[[[0, 0], [4, 4], [8, 8]],
                                      [[1, 1], [2, 2], [5, 5]]],
                                     [[[0, 0], [4, 4], [8, 8]],
                                      [[1, 1], [2, 2], [5, 5]]]]
        default.params["freqs"] = range(nfreqs)
        default.params["figsize"] = (8, 5)
        default.params["title"] = ("Bare Susceptibilities in particle-hole "
                                   "(ph) and particle-particle (pp) channels")
        default.params["titlesize"] = 14
        xlim = [0, nqpath-1]
        default.params["xlim"] = [[xlim, xlim], [xlim, xlim]]
        default.params["ylim"] = None
        default.params["xlabels"] = None
        ylabel1 = r"$[\chi^0_{ph}(i\nu_{[&v]},\vec{q})]_{l_1l_2}$"
        ylabel2 = r"$[\chi^0_{pp}(0)]_{i\omega_0,\vec{k},l_1l_2}$"
        default.params["ylabels"] = [[ylabel1, None], [ylabel2, None]]
        default.params["labelsize"] = 16
        default.params["ticksize"] = 12
        xpath = ([[0, (nqpath-1)/3-1, (nqpath-1)*2/3-1, nqpath-1],
                 [r"$\Gamma$", "M", "X", r"$\Gamma$"]])
        default.params["xticks"] = [[xpath, xpath], [xpath, xpath]]
        default.params["yticks"] = None
        default.params["plot_labels"] = [[["xy;xy", "yz;yz", "zx;zx"],
                                          ["xy;yz", "xy;zx", "yz;zx"]],
                                         [["xy;xy", "yz;yz", "zx;zx"],
                                          ["xy;yz", "xy;zx", "yz;zx"]]]
        default.params["plot_colors"] = [[["blue", "red", "green"],
                                          ["black", "orange", "purple"]],
                                         [["blue", "red", "green"],
                                          ["black", "orange", "purple"]]]
        default.params["tight_layout"] = False
        default.params["show"] = False
        default.params["legend_loc"] = "best"

        plotParams.setDefaults(default)

        # Plotting a figure for each bosonic frequency
        for v in plotParams["freqs"]:
            # Construct figure and subplot axes
            fig, ax = plotting.setFigure(plotParams)

            # Loading Re parts of Bare Susceptibilities in ph and pp channels
            chi0_ph = self.ReturnBareSusph(nqpath)
            chi0_pp = self.ReturnBareSuspp(nqpath)

            BareSusph = chi0_ph.Re
            BareSuspp = chi0_pp.Re

            BareSuspp.Obj *= -1

            # Plotting requiered matrix elements
            plotting.plotPath(ax, 0, 0, BareSusph, v, plotParams)
            plotting.plotPath(ax, 0, 1, BareSusph, v, plotParams)
            plotting.plotPath(ax, 1, 0, BareSuspp, v, plotParams)
            plotting.plotPath(ax, 1, 1, BareSuspp, v, plotParams)

            # Change variable-dependant text from labels
            plotting.setSubplot(ax, plotParams,
                                printVariables={"v": str(v)})
            # Plot vertical dashed lines for esthetics
            for a in [0, 1]:
                for b in [0, 1]:
                    xticks = plotParams["xticks"][0][0][0]
                    for i in range(1, len(xticks)-1):
                        plotting.plotVerticalLine(ax[a][b], xticks[i],
                                                  plotParams)

            # Set savefile's path and show or draw
            saveplace = (self.conn.groundDir + self.baseDir + "BareSus_v" +
                         str(v).zfill(2) + ".png")
            plotting.finish(plotParams, saveplace)

    def PlotBareSusGrid(self, mesh, params=None):
        if self.id == -1:
            raise("Error: no id.")

        if len(self.cells) == 0:
            self.cells = self.Load(True)

        # nfreqs = self.cells["barechi2.nivn"] + 1

        default = PlotParams()
        default["subplots"] = [2, 3]
        default["indices"] = [[[0, 0], [4, 4], [8, 8]],
                              [[0, 0], [4, 4], [8, 8]]]
        default["freqs"] = [0]
        default["figsize"] = (12, 8)
        default["title"] = ""
        default["titlesize"] = 14
        xlim = [0, 2*mesh.n[0]-1]
        ylim = [0, 2*mesh.n[1]-1]
        default["xlim"] = [[xlim, xlim, xlim], [xlim, xlim, xlim]]
        default["ylim"] = [[ylim, ylim, ylim], [ylim, ylim, ylim]]
        default["xlabels"] = [None]
        default["ylabels"] = [None]
        default["labelsize"] = 12
        default["xticks"] = [None]
        default["yticks"] = [None]
        default["ticksize"] = 10
        default["cmap"] = [["Blues", "Blues", "Blues"],
                           ["Reds_r", "Reds_r", "Reds_r"]]
        default["bar_format"] = [["%01.01e", "%01.01e", "%01.01e"],
                                 ["%01.01e", "%01.01e", "%01.01e"]]
        default["bar_ticksize"] = 10
        default["tight_layout"] = False
        default["show"] = False
        default["legend_loc"] = None
        default["special1"] = False
        default["special2"] = False
        default["Trace"] = 0

        default["sharey"] = False

        if params is None:
            params = PlotParams()
        params.setDefaults(default)

        for v in params["freqs"]:
            fig, ax = plotting.setFigure(params)

            chi0_ph = self.ReturnBareSusph(mesh.nkpt)
            chi0_pp = self.ReturnBareSuspp(mesh.nkpt)

            BareSusph = chi0_ph.Re
            BareSuspp = chi0_pp.Re

            for b in range(params["subplots"][1]):
                plotting.plotGrid(ax, 0, b, BareSusph, v, mesh, params)
                plotting.plotGrid(ax, 1, b, BareSuspp, v, mesh, params)

                if params["special1"]:
                    plotting.plotSpecial1(ax[0, b], mesh.n, params)
                    plotting.plotSpecial1(ax[1, b], mesh.n, params)
                if params["special2"]:
                    plotting.plotSpecial2(ax[0, b], mesh.n, params)
                    plotting.plotSpecial2(ax[1, b], mesh.n, params)

            plotting.setSubplot(ax, params)

            saveplace = (self.conn.groundDir + self.baseDir + "BareSus_v" +
                         str(v).zfill(2) + ".png")
            plotting.finish(params, saveplace)
